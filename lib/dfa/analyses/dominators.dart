import 'package:sca23/cfg/cfg.dart';
import 'package:sca23/dfa/analysis.dart';

class DominatorAnalysis extends IntraproceduralForwardAnalysis<Set<int>> {
  DominatorAnalysis(IntraproceduralControlFlowGraph cfg) : super(cfg);
  @override
  Set<int> get initElement {
    var newSet = <int>{};
    newSet.add(cfg.entryNode.id);
    return newSet; }
  @override
  Set<int> get oneElement {return cfg.nodes.map((node) => node.id).toSet(); }
  @override
  Set<int> apply(Node node, Set<int> incoming) {
    var newSet = <int>{};
    newSet = newSet.union(incoming);
    newSet.add(node.id);
    return newSet;
   }
  @override
  bool equal(Set<int> left, Set<int> right) {
    return (left.length == right.length && left.every((i) => right.contains(i)));
  }
  @override
  Set<int> merge(Set<int> left, Set<int> right) {
    return left.intersection(right);
   }
}