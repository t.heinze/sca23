import 'package:sca23/dfa/analyses/definitions.dart';
import 'package:sca23/dfa/analyses/liveness.dart';
import 'package:sca23/dfa/solver.dart';
import 'package:sca23/cfg/cfg.dart';

class UDChains {
  final variablesUsesCollector = VariableUsesCollector();
  final Map<Node, Set<Definition>> definitions;

  UDChains(IntraproceduralControlFlowGraph cfg)
      : definitions = WorklistSolver().solve(ReachingDefinitionsAnalysis(cfg));

  Map<Node, Set<Definition>> get chains =>
      {for (var node in definitions.keys) node: chain(node)};

  Set<Definition> chain(Node node) => node.predecessors
      .fold<Set<Definition>>(
          {}, (value, predecessor) => value.union(definitions[predecessor]!))
      .where((definition) =>
          node.accept(variablesUsesCollector).contains(definition.variable))
      .toSet();
}
