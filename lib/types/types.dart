import 'package:sca23/ast/printer.dart';
import 'package:sca23/ast/ast.dart';

abstract class Term {}

class TypeVariable extends Term {
  static var counter = 0;
  final int id = counter++;
  TypeVariable();
  @override
  String toString() => 'α$id';
}

class ExpressionVariable extends TypeVariable {
  static final printer = Printer();
  final Expression node;
  ExpressionVariable(this.node);
  @override
  String toString() => '[[${printer.visitNode(node)}]]';
}

class IntegerType extends Term {
  IntegerType._();
  static final IntegerType singleton = IntegerType._();
  factory IntegerType() => singleton;
  @override
  String toString() => 'int';
}

class FunctionType extends Term {
  final List<Term> parameterTypes;
  final Term returnType;
  FunctionType(this.parameterTypes, this.returnType);
  @override
  String toString() => '(${parameterTypes.join(',')})->$returnType';
}

class ArrayType extends Term {
  final Term elementType;
  ArrayType(this.elementType);
  @override
  String toString() => '$elementType[]';
}
