// ignore_for_file: constant_identifier_names

enum Token {
  LPAREN,
  RPAREN,
  LBRACE,
  RBRACE,
  LSQUARE,
  RSQUARE,
  PLUS,
  MINUS,
  STAR,
  SLASH,
  NOT,
  LT,
  GT,
  EQ,
  NE,
  SEMICOLON,
  COMMA,
  ASSIGN,
  IF,
  ELSE,
  WHILE,
  RETURN,
  IDENTIFIER,
  INTEGER,
  EOS,
  OTHER
}

const keywords = <String, Token>{
  'if': Token.IF,
  'else': Token.ELSE,
  'while': Token.WHILE,
  'return': Token.RETURN
};

Token lookup(String syntax) => keywords[syntax] ?? Token.IDENTIFIER;
